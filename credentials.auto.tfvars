/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:

        Any files with names ending in .auto.tfvars or .auto.tfvars.json are loaded automatically

        A .tfvars file is used to assign values to variables that have already been declared in .tf files, 
        not to declare new variables. Review the file `variables.tf` for declared variables.

        Single quotes are not valid. Use double quotes (") to enclose strings.

        You can define variable values in environment variables, but the precedence is the last value found,

        To specify the APIC password as an environment variable, issue the shell command:

        $ export TF_VAR_apic_password="changeme"

        The variable `apic_username` is specified in this file, the associated password will be provided as an
        environment variable.

    references:
      - https://www.terraform.io/docs/configuration/variables.html#assigning-values-to-root-module-variables
      - https://www.terraform.io/docs/configuration/variables.html#variable-definition-precedence

*/

apic_username = "admin"
