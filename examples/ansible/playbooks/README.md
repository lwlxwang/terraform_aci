README.md
---------

The playbook `terraform_main.yml` is an example of using an Ansible playbook to execute Terraform and access the returned output in subsequent tasks in the playbook.