/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:

      Example of using the local provider to create a file and execute Ansible. Normally you would want to
      define the provisioner under the resource you wish to provison using Ansible. In this example, the local provider
      may execute before or after the resources specified in the `resources.tf` file.

      Destroy will delete the file specified.

    references:
      - https://github.com/scarolan/ansible-terraform
      - https://registry.terraform.io/providers/hashicorp/local/latest/docs
      - https://github.com/sethvargo/terraform-provider-filesystem

*/  

resource "local_file" "foo" {
    content     = "foo!"
    filename = "/tmp/foo/bar"

    # filename = "${path.module}/foo.bar"

  provisioner "local-exec" {
    command = "ansible-playbook --version"
  }
}