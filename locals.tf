/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:

      A local value assigns a name to an expression, so you can use it multiple times within a module without repeating it.

      In this example, two variables are defined, to identify the engineer initiating the workflow and a reference to the 
      service request ticket.

    references:
      - https://www.terraform.io/docs/configuration/locals.html

*/

locals {
  initiator = "Joel W. King @joelwking"
  service_request = "CHG0123456"
}